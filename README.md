Bitbucket Issues CSV Converter
==============================

A crude converter that turns a Bitbucket issues.zip into an Excel-dialect csv file.

You point it to a zip file you downloaded from Bitbucket and it writes out a CSV file that contains the issues (just the issues).

Usage
=====

    :::text
    $ git clone git@bitbucket.org:evzijst/bbcsv.git
    Cloning into 'bbcsv'...
    $ cd bbcsv/
    bbcsv$ python jsontocsv.py ~/Downloads/dogslow-issues.zip -o issues.csv

Here it is in action:

[http://youtu.be/uw4IJhsaQfI](http://youtu.be/uw4IJhsaQfI)
