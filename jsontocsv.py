import json
import sys
import unicodecsv
import zipfile

if len(sys.argv) < 2:
    print >> sys.stderr, 'Usage: $ python %s issues.zip [-o FILE]' % sys.argv[0]
    exit(1)

with zipfile.ZipFile(sys.argv[1], mode='r') as zf:
    issues = json.loads(zf.read('db-1.0.json'))['issues']

out = (len(sys.argv) == 4) and open(sys.argv[3], 'w') or sys.stdout
cols = ['id', 'title', 'kind', 'status', 'priority', 'reporter', 'assignee',
        'created_on', 'edited_on', 'updated_on', 'content_updated_on',
        'version', 'milestone', 'component', ]
writer = unicodecsv.DictWriter(out, cols, extrasaction='ignore')
writer.writerow({col: col for col in cols})
writer.writerows(sorted(issues, key=lambda issue: issue['id'], reverse=True))
